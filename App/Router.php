<?php

namespace App;

require_once __DIR__ . "/../SON/Init/Bootstrap.php";

class Router extends \SON\Init\Bootstrap
{
    protected function initRoutes()
    {
        $routes["home"] = array(
            "route" => "/", "controller" => "indexController", "action" => "index"
        );

        $routes["contact"] = array(
            "route" => "/contact", "controller" => "indexController", "action" => "contact"
        );

        $this->setRoutes($routes);
    }
}