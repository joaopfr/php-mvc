<?php

namespace App\DAO;

require_once __DIR__ . "/../Model/Cliente.php";
require_once __DIR__ . "/../../SON/DAO/ModelDAO.php";

use SON\DAO\ModelDAO;

class ClienteDAO extends ModelDAO
{
    public function __construct(\PDO $db)
    {
        parent::__construct($db, "cliente");
    }

    public function list()
    {
        $clientesResult = parent::list();
        foreach ($clientesResult as $clienteRow)
        {
            $cliente = new \App\Model\Cliente;
            $cliente->setId($clienteRow["id"])
                    ->setName($clienteRow["name"])
                    ->setEmail($clienteRow["email"]);
            $clientes[] = $cliente;
        }
        return $clientes;
    }

    public function find($id)
    {
        $clienteResult = parent::find($id);
        $cliente = new \App\Model\Cliente;
        $cliente->setId($clienteResult["id"])
                ->setName($clienteResult["name"])
                ->setEmail($clienteResult["email"]);
        return $cliente;
    }
}