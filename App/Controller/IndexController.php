<?php

namespace App\Controller;

require_once __DIR__ . "/../../SON/Controller/Action.php";
require_once __DIR__ . "/../../SON/DI/Container.php";

class IndexController extends \SON\Controller\Action
{
    public function index()
    {
        $clienteDAO = \SON\DI\Container::getDAO("ClienteDAO");
        $this->view->clientes = $clienteDAO->list();
        $this->render("index");
    }

    public function contact()
    {
        $clienteDAO = \SON\DI\Container::getDAO("ClienteDAO");
        $this->view->clientes = array($clienteDAO->find("1"));
        $this->render("contact");
    }
}