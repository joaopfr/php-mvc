<?php

namespace App;

class Conn
{
    public static function getDb()
    {
        $host = "172.18.0.2";
        $user = "root";
        $pass = "";
        $db = "mvc";

        return new \PDO("mysql:host={$host};dbname={$db}", $user, $pass);
    }
}