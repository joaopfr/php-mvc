<?php

namespace SON\DI;

require_once __DIR__ . "/../../App/Conn.php";

use App\Conn;

class Container
{
    public static function getDAO($dao)
    {
        $classFullname = "\\App\\DAO\\" . ucfirst($dao);
        $classFullpath = __DIR__ . "/../../App/DAO/" . ucfirst($dao) . ".php";
        require_once $classFullpath;
        return new $classFullname(Conn::getDb());
    }
}