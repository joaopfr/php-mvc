<?php

namespace SON\Controller;

abstract class Action
{
    protected $view;
    private $action;

    public function __construct()
    {
        $this->view = new \stdClass;
    }

    protected function render($action, $hasLayout = true)
    {
        $this->action = $action;
        $layoutPath = __DIR__ . "/../../App/View/layout.phtml";
        if ($hasLayout && file_exists($layoutPath))
        {
            include_once $layoutPath;
        }
        else
        {
            $this->content();
        }
    }

    protected function content()
    {
        $classFullName = get_class($this);
        $className = str_replace("App\\Controller\\", "", $classFullName);

        $viewDir = strtolower(str_replace("Controller", "", $className));
        include_once __DIR__ . "/../../App/View/{$viewDir}/{$this->action}.phtml";
    }
}