<?php

namespace SON\DAO;

abstract class ModelDAO
{
    private $db;
    private $model;

    public function __construct(\PDO $db, $model)
    {
        $this->db = $db;
        $this->model = $model;
    }

    protected function list()
    {
        $sql = "select * from {$this->model}";
        return $this->db->query($sql);
    }

    protected function find($id)
    {
        $sql = "select * from {$this->model} where id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $ret = $stmt->fetch(\PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $ret;
    }
}