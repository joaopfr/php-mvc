<?php

namespace SON\Init;

require_once __DIR__ . "/../../App/Controller/IndexController.php";

abstract class Bootstrap
{
    private $routes;

    public function __construct()
    {
        $this->initRoutes();
        $this->run($this->getUrl());
    }

    abstract protected function initRoutes();
    
    private function run($url)
    {
        array_walk($this->routes, function($route) use ($url)
        {
            if ($url == $route["route"])
            {
                $class = "App\\Controller\\" . ucfirst($route["controller"]);
                $controller = new $class;

                $action = $route["action"];
                $controller->$action();
            }
        });
    }

    protected function setRoutes($routes)
    {
        $this->routes = $routes;
    }

    private function getUrl()
    {
        return parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
    }
}